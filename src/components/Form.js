import './Form.scss';
import { useState } from 'react';

const Form = (props) => {
  const [email, setEmail] = useState('');

  const handleSubmit = (e) => {
    setEmail('');
    alert(`✓ Email has been successfully sent to ${email}`);
    e.preventDefault();
  }

  return (
    <div className="Form">
      <form className="form-group p-5 d-flex justify-content-center flex-column" onSubmit={(e) => handleSubmit(e)}>
        <ul className="d-flex flex-row ps-0">
          {props.selectedItems.map(item => (
            <li key={item} className="pe-3">#{item}</li>
          ))}
        </ul>
        <label htmlFor="email" className="pb-2">Email</label>
        <input 
          type="email" 
          className="form-control" 
          name="email" 
          id="email" 
          value={email}
          aria-describedby="emailHelpId" 
          placeholder="Please enter your email" 
          disabled={props.selectedItems.length > 0 ? false : true} 
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <small id="emailHelpId" className="form-text text-muted">Please select at least one country</small>
        <button type="submit" className="btn btn-custom mt-4" disabled={props.selectedItems.length > 0 ? false : true}>Submit</button>
      </form>
    </div>
  );
}

export default Form;