import './Countries.scss';
import React, { useState, useEffect } from 'react';

const Countries = (props) => {
    const [error, setIsError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    
    useEffect(() => {
        fetch("https://restcountries.com/v3.1/all")
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setItems(result);
            },
            (error) => {
              setIsError(true);
              setIsLoaded(true);
            }
          )
    }, []);

    /**
     * 
     * @param {String} name 
     */
    const selectItem = (name, e) => {
        /* Fill selected items array */
        e.target.classList.toggle('selected');

        /* Return array to parent component */
        props.parentCallback(name, e);
        e.preventDefault();
    }

    if(error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return(
            <div className="Countries h-100 d-flex justify-content-center align-items-center">
                <h3>Loading...</h3>
            </div>
        ) 
    } else {
        return (
            <ul className="p-3 Countries">
                {items.map(item => (
                    <li key={item.name.common} className="ps-2 pe-4 d-flex flex-row align-items-center justify-content-between py-3 country__item" onClick={(e) => selectItem(item.name.common, e)}>
                        <div className="d-flex flex-row align-items-center">
                            <img src={item.flags.png} alt={'flag of ' + item.name.common}  width="24px"></img>
                            <p className="ms-3 mb-0">{item.name.common}</p>
                        </div>
                        <p className="mb-0 justify-self-end selected__icon">✓</p>
                    </li>
                ))}
            </ul>
        )
    }
}

export default Countries; 
