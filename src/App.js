import './App.scss';
import Countries from './components/Countries';
import Form from './components/Form';
import { useState } from 'react';

const App = () => {
  const [selectedItems, setSelectedItems] = useState([]);
  
  const handleCallback = (childData, e) => {
    if(e.target.classList.contains('selected')) {
      setSelectedItems([childData, ...selectedItems]);
    } else {
      setSelectedItems(selectedItems.filter(e => e !== childData ));
    }
  }

  return (
    <div className="App">
      <div className="row g-0">        
        <div className="col-md-6 order-md-0 order-1">
          <Countries parentCallback={handleCallback}/>
        </div>
        <div className="col-md-6">
          <Form selectedItems={selectedItems}/>
        </div>
      </div>
    </div>
  );
}

export default App; 
